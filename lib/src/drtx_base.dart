class X {
  /// if a string is has 13 characters or more it return the first 5 characters
  /// with 3 (...) dots and the last 5 characters
  static String midot(String str) {
    if (str.length > 20) {
      final start = str.substring(0, 12);
      final end = str.substring(str.length - 4);
      return "$start...$end";
    }
    return str;
  }
}
