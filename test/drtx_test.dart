import 'package:drtx/drtx.dart';
import 'package:test/test.dart';

void main() {
  group('StrFmt tests', () {
    setUp(() {
      // Additional setup goes here.
    });

    test('shrtMidDot', () {
      expect(X.midot("0123456789"), "0123456789");
      expect(X.midot("0123456789ABCDE"), "01234...ABCDE");
      expect(
          X.midot(
              "0123456kfjdffdfaunfdfnhjfdhfdfdkjdfdfkdfnngfdfjgghjfghfjhgffghfghhjfghjfgjhfghfghjfghgjfghjfghjfghrbebqjewe83929odf;****3774743g789ABCDE"),
          "01234...ABCDE");
    });
  });
}
